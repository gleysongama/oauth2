package br.com.socialticket.oauth2.dto;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import br.com.socialticket.oauth2.model.Usuario;

import java.util.Collection;

public class UsuarioCustomDTO implements UserDetails {

	private static final long serialVersionUID = 1L;
	private String login;
    private String senha;

    public UsuarioCustomDTO(Usuario usuario) {
		super();
		this.login = usuario.getLogin();
		this.senha = usuario.getSenha();
	}

	@Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return null;
    }

    @Override
    public String getPassword() {
        return senha;
    }

    @Override
    public String getUsername() {
        return login;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }
}