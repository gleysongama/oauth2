package br.com.socialticket.oauth2.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import br.com.socialticket.oauth2.model.Usuario;
import br.com.socialticket.oauth2.repository.UsuarioRepository;

@Service
public class UsuarioService {

	@Autowired
    private UsuarioRepository repository;
	
	public Usuario save(Usuario usuario) {
		return repository.save(usuario);
	}
	
	public Iterable<Usuario> findAll() {
		return repository.findAll();
	}
	
	public Page<Usuario> findPerPage(int page, int itensPerPage) {
		Pageable pageable = PageRequest.of(page, itensPerPage);
		return repository.findAll(pageable);
	}
	
	public Page<Usuario> findPerPageOrderBy(int page, int itensPerPage, String propertyName) {
		Sort sort = Sort.by(propertyName).ascending();
		Pageable pageable = PageRequest.of(page, itensPerPage, sort);
		return repository.findAll(pageable);
	}
	
	public Optional<Usuario> findById(Long id) {
		return repository.findById(id);
	}
}
