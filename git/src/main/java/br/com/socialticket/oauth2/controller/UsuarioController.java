package br.com.socialticket.oauth2.controller;

import java.util.Objects;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.socialticket.oauth2.model.Usuario;
import br.com.socialticket.oauth2.service.UsuarioService;

@RestController
@RequestMapping("/usuarios")
public class UsuarioController {

    @Autowired
    private UsuarioService usuarioService;
    
    @PostMapping
    public ResponseEntity<?> save(@RequestBody Usuario usuario) {
    	usuario.setSenha(new BCryptPasswordEncoder().encode(usuario.getSenha()));
    	return ResponseEntity
    			.status(HttpStatus.OK)
    			.body(usuarioService.save(usuario));
    }
    
    @GetMapping
    public ResponseEntity<Iterable<?>> findAll() {
    	return ResponseEntity
    			.status(HttpStatus.OK)
    	        .body(usuarioService.findAll());
    }
    
    @GetMapping("/page/{page}/itens-per-page/{itensPerPage}")
    public ResponseEntity<Iterable<?>> findPerPage(@PathVariable int page, @PathVariable int itensPerPage) {
    	return ResponseEntity
    			.status(HttpStatus.OK)
    	        .body(usuarioService.findPerPage(page, itensPerPage));
    }
    
    @GetMapping("/page/{page}/itens-per-page/{itensPerPage}/property-name/{propertyName}")
    public ResponseEntity<Iterable<?>> findPerPageOrderBy(@PathVariable int page, @PathVariable int itensPerPage, @PathVariable String propertyName) {
    	return ResponseEntity
    			.status(HttpStatus.OK)
    	        .body(usuarioService.findPerPageOrderBy(page, itensPerPage, propertyName));
    }
    
    @GetMapping("/{id}")
    public ResponseEntity<?> findById(@PathVariable Long id) {
    	Optional<Usuario> usuario = usuarioService.findById(id);
    	if(Objects.isNull(usuario)) {
    		return ResponseEntity.badRequest().build();
    	}
    	return ResponseEntity
    			.status(HttpStatus.OK)
    			.body(usuario);
    }

}