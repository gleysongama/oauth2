Este projeto é uma implementação do padrão OAUTH2 baseado [neste link](https://academiadev.gitbook.io/joinville/seguranca/oauth2).

## Roteiro de utilização do projeto

### `Regras iniciais`

Para efeito de testes é permitido criar usuários sem estar autenticado.

Não é permitido listar usuários sem estar autenticado

### `Autenticação`

Para realizar autenticação (POSTMAN ou INSOMNIA) faça uma requisição GET para http://localhost:8080/oauth/token?grant_type=password&username=admin&password=admin.

Informe, em modo Basic de autenticação, as credenciais públicas: username=client-id e password=secret-id.

Em caso positivo a resposta do GET request será: "access_token", "token_type", "refresh_token", "expires_in", "scope".

Copie o "access_token" e em modo Bearer, cole no campo Token e digite Bearer no campo Prefix.

Após o precedimento acima e de posse do "access_token", você está autorizado para fazer requisições GET para listagem de usuários.

### `URI POST e BODY REQUEST`

URI: [http://localhost:8080/usuarios/](http://localhost:8080/usuarios/)

Body:
 
{	
		
  "login": "anapaula",  
  "senha": "anapaula"  
}

### `URI GET`

URI: [http://localhost:8080/usuarios](http://localhost:8080/usuarios)

TOKEN: *&%$#@

PREFIX: Bearer
